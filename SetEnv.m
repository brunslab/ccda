function SetEnv()
    % Camera Control and Data Acquisition (CCDA) V3
    % Copyright (C) 2020  BrunsLab HPC
    %
    % This program is free software: you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation, either version 3 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
    % Determine where your m-file's folder is.
    folder = fileparts(which(mfilename)); 
    % Add that folder plus all subfolders to the path.
    addpath(genpath(folder));
    % Start logging user interactions for debugging
    diary ([sprintf('%s/Diaries/',pwd),sprintf('BrunsLab_CCDA_%s.txt', datestr(now,'mm-dd-yyyy HH-MM-SS'))]); 
    diary on;
    