function [status,message] = changeCameraProperty(multi,propName,propVal)
%CHANGECAMERAPROPERTY Safe camera property modifications

% Camera Control and Data Acquisition (CCDA) V3
% Copyright (C) 2020  BrunsLab HPC
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <https://www.gnu.org/licenses/>.
src = multi.Src;
prinfo = propinfo(src,propName);
if strcmp(prinfo.ReadOnly,"NotCurrently")
    if multi.IsAcquiring
        status = 3;
        message = sprintf("Cannot change value of %s while acquiring",propName);
        return;    
    end
elseif ~strcmp(class(prinfo.DefaultValue),class(propVal))
    status = 1;
    message = sprintf('Wrong data type: wanted %s, got %s',...
                      prinfo.Type,class(propVal));
    return;
elseif strcmp(prinfo.Constraint,"bounded")
    if propVal>prinfo.ConstraintValue(1) && propVal<prinfo.ConstraintValue(2)
        set(src,propName,propVal);
    else
        status = 2;
        message = sprintf("Error: Value %s not in range (%s %s)",...
            propVal,string(prinfo.ConstraintValue(1)),string(prinfo.ConstraintValue(2)));
        return;
    end
elseif strcmp(prinfo.Constraint,"enum")
    if any(strcmp(string(prinfo.ConstraintValue), propVal))
        set(src,propName,propVal);
    else
        status = 2;
        enumVals = sprintf("%s; ",string(prinfo.ConstraintValue));
        message = sprintf("Error: Value %s not within options: \n%s of %s",...
                          propVal,enumVals,propName);
        return;
    end
end


status = 0;
message = sprintf('Value of property %s succesfully changed to %s',...
                  propName,string(propVal));

