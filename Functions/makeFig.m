function [newfig,newimg] = makeFig(num,vidRes,nBands)
%makeFig: Creates new figure and img to display a single acquisiton channel preview

    % Camera Control and Data Acquisition (CCDA) V3
    % Copyright (C) 2020  BrunsLab HPC
    %
    % This program is free software: you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation, either version 3 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program.  If not, see <https://www.gnu.org/licenses/>.

    newfig = figure('Toolbar','none',...
        'Menubar','none',...
        'NumberTitle','Off',...
        'Name', sprintf('Channel %d',num),...
        'Units', 'normalized',...
        'Position', [0.70,0.30-0.05*num,0.25,0.35],...
        'Colormap','default');
    
    
    newfig.Units = 'pixels';
    newfig.Position = [newfig.Position(1) newfig.Position(2) ...
                       vidRes(1)+40 vidRes(2)+25];
                   
    newimg = image( zeros(vidRes(2), vidRes(1), nBands), 'CDataMapping', 'direct');
%     imgs(num).CDataMapping = 'direct';
    ax = newfig.CurrentAxes;
    set(ax,'visible','off');
    set(ax,'NextPlot','replace');
    set(ax,'Units','pixels');
    set(ax,'Position',[0 1 newfig.InnerPosition(3)-30 ...
        (newfig.InnerPosition(3)-30)*vidRes(2)/vidRes(1)]);
%     set(ax,'Position', [0.01,0.01,0.9,0.9]);
%     set(ax,'Position',[0.07,0.02,0.8525,0.9625]);

    colormap('Gray');
    slider = uicontrol(newfig,'Style','slider',...
                       'Callback',@slider_callback,...
                       'Value',0.5,...
                       'Max',5,...
                       'Min',0.01,...
                       'Position',[newfig.InnerPosition(3)-25 ...
                       newfig.InnerPosition(4)/2-160 20 320]);
    newfig.Units = 'pixels';
    data.slider = slider.Value;
    guidata(newimg,data);
    
%     newimg = imgs(num);
    
    
    function slider_callback(hObject,~)
        data = guidata(newimg);
        data.slider = hObject.Value;
        guidata(newimg,data);
    end
end
