function myacq_fcn(obj,event,multi)
% myacq_fcn: A handler for frames acquired event of matlab Video object

    % Camera Control and Data Acquisition (CCDA) V3
    % Copyright (C) 2020  BrunsLab HPC
    %
    % This program is free software: you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation, either version 3 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program.  If not, see <https://www.gnu.org/licenses/>.

    channels = length(multi.Imgs);
    [eventData, timeData, metaData] = getdata(obj,multi.Channels);
    
    if multi.IsAcquiring
        multi.WriteToDisk(eventData);
    end
    timediff = etime(datevec(now),metaData(1).AbsTime);
    if ~multi.IsMultiviewing  || timediff>5
        return
    end
    try
        for framenum=1:channels
            ch = mod(metaData(framenum).FrameNumber,channels);
            if ch == 0
                ch = channels;
            end
            uidata = guidata(multi.Imgs(ch));
            bfactor = uidata.slider;
            cdata = bfactor*im2uint8(eventData(:,:,:,framenum));
            
             if multi.IsMultiplexing
                data = guidata(multi.Muximg);
                for rgbch=1:length(data.Colors)
                    if data.Colors(rgbch)==ch
                       muxdata(:,:,rgbch) = cdata;
                    end
                end
             end
%             else
                set(multi.Imgs(ch),'CData',cdata);
                drawnow limitrate;
%             end
        end
        if multi.IsMultiplexing
            for rgbch=1:length(data.Colors)
                if data.Colors(rgbch) > length(multi.Imgs)
                    muxdata(:,:,rgbch) = zeros(size(eventData(:,:,:,ch)));
                end
            end
            multi.Muximg.CData = muxdata;
        end
        drawnow limitrate;
    catch E
        fprintf('\n### Error in custom acquisition function (used for multiview) \n%s\n',E.message);
    end
end