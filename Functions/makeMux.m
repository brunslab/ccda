function muximg = makeMux(vidRes,imgs)
%makeMux: Creates creates new figure and image for multiplex preview

    % Camera Control and Data Acquisition (CCDA) program
    % Copyright (C) 2020  BrunsLab HPC
    %
    % This program is free software: you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation, either version 3 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program.  If not, see <https://www.gnu.org/licenses/>.

    newfig = figure('Toolbar','none',...
        'Menubar','none',...
        'NumberTitle','Off',...
        'Name', 'Mux Channel',...
        'Units', 'normalized',...
        'Position', [0.70,0.05,0.25,0.35],...
        'Colormap','default');
    newfig.Units = 'pixels';
    try
        newfig.Position = [newfig.Position(1) newfig.Position(2) ...
                           vidRes(1) vidRes(2)+65];
        muximg = image( zeros(vidRes(2), vidRes(1), 3) );
        muximg.CDataMapping = 'direct';
        ax = newfig.CurrentAxes;
        set(ax,'visible','off');
        set(ax,'NextPlot','replace');
        set(ax,'Units','pixels');
        set(ax,'Position',[0 40 newfig.InnerPosition(3) ...
            (newfig.InnerPosition(3))*vidRes(2)/vidRes(1)]);

        channels = strings(length(imgs),1);
        data = guidata(muximg);
        data.Colors = 1:3;%RGB are channels 1 2 3
        for i = 1:length(imgs)
            channels(i) = sprintf('Channel %d',i);
        end
        for i = data.Colors
            SetColormap(i);
        end
        channels(length(imgs)+1) = 'None';

        popupwidth = 100;
        red = uicontrol(newfig,'Style','popupmenu');
        red.Position = [newfig.InnerPosition(3)/4-popupwidth/2 10 popupwidth 20];
        red.String = channels;
        red.Callback = {@channel_selection,1};
        red.Value = 1;
        redlabel = uicontrol(newfig,'Style','text','String','Red: ',...
            'Position', [newfig.InnerPosition(3)/4-popupwidth 10 40 20]);

        green = uicontrol(newfig,'Style','popupmenu');
        green.Position = [newfig.InnerPosition(3)/2-popupwidth/2 10 popupwidth 20];
        green.String = channels;
        green.Callback = {@channel_selection,2};
        green.Value = 2;
        greenlabel = uicontrol(newfig,'Style','text','String','Green: ',...
            'Position', [newfig.InnerPosition(3)/2-popupwidth 10 40 20]);

        blue = uicontrol(newfig,'Style','popupmenu');
        blue.Position = [newfig.InnerPosition(3)*3/4-popupwidth/2 10 popupwidth 20];
        blue.String = channels;
        blue.Callback = {@channel_selection,3};
        blue.Value = 3;
        bluelabel = uicontrol(newfig,'Style','text','String','Blue: ',...
            'Position', [newfig.InnerPosition(3)*3/4-popupwidth 10 40 20]);

    %     uicontrol(gcf,'Style','togglebutton','Callback',@toggle_callback_H,'Value',0,'Max',1,'Min',0,'Position',[520 30 40 20],'String','H Flip');
    %     value.toggle_H = 0;
    %     uicontrol(gcf,'Style','togglebutton','Callback',@toggle_callback_V,'Value',0,'Max',1,'Min',0,'Position',[520 5 40 20],'String','V Flip');
    %     value.toggle_V = 0;
    %     guidata(newimg,datas);

        guidata(muximg,data);
    catch E
        close(newfig);
        rethrow(E);
    end

    function channel_selection(channelPopup,event,color)
        data = guidata(muximg);
        if sum(data.Colors==data.Colors(color))==1 && color<=length(imgs)
            colormap(imgs(color).Parent,'Gray');
            slider = imgs(color).Parent.Parent.Children(1);
            slider.Value = 0.5;
            slider.Callback(slider);
        end
        data.Colors(color) = channelPopup.Value;
        SetColormap(color);
        guidata(muximg,data);
        for j=1:3
            SetColormap(j);
        end
    end

    function SetColormap(channel)
        if data.Colors(channel)<=length(imgs)
            cmap = zeros(256,3);
            cmap(:,channel) = linspace(0,1,256);
            colormap(imgs(data.Colors(channel)).Parent,cmap);
            slider = imgs(data.Colors(channel)).Parent.Parent.Children(1);
            slider.Value = 2.5;
            slider.Callback(slider);
            
        end
    end
end