classdef Multiviewer < handle
    % Multiviewer - a class for handling and visualizing multiple channels 
    % from a camera (AlliedVision GoldEye G-032, other GigE cameras in principle)
    %   Launch separate preview windows (multiview) for separate channels,
    %   multiplex them into an RGB image, acquire images using a fast TIFF 
    %   writer.

    % Camera Control and Data Acquisition (CCDA) V3
    % Copyright (C) 2020  BrunsLab HPC
    %
    % This program is free software: you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation, either version 3 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
    properties
        Vid;
        Src;
        IsMultiviewing = false;
        IsAcquiring = false;
        IsMultiplexing = false;
        Imgs;
        Figs;
        Muximg;
        Channels = 3;
        
        TiffWriter;
    end
    
    methods
        function multi = Multiviewer(vid,src)
            %Constructor Construct an instance of Multiviewer
            %   By default uses the first gige source with Mono14 color
            %   depth setting
            SetEnv;
            if nargin == 0
                [vid,src] = SetupVideoInput('Mono14');
            end
            multi.Vid = vid;
            multi.Src = src;
            multi.Imgs = gobjects(0,1);
            
            multi.Vid.FramesAcquiredFcnCount = multi.Channels;
            multi.Vid.FramesAcquiredFcn = {@myacq_fcn,multi};
        end
        
        function StartAcquiring(multi,DataFolder,FileDescriptor,OperatingMode,ArduinoFile)
            if ~multi.IsAcquiring
                fprintf('\n### Preparing for data acquisition...');
                Filename = sprintf('BrunsLab_CCDA_%s', datestr(now,'mm-dd-yyyy HH-MM-SS')); % Specify the file name here 
                Filename = strcat(Filename,'_',FileDescriptor);% append file name description
                Fullpath = fullfile(DataFolder,Filename);
                % Get configuration data and save to json
                src_struct = get(multi.Src); % return obj as struct
                src_struct.Parent = 1;

                GenConfig =  GeneralConfig(Filename, OperatingMode);
                TrigConfig = TriggerConfig(ArduinoFile);
                JSONConfig(GenConfig,TrigConfig,src_struct,strcat([Fullpath,'.json']));
            %     JSONConfig(GenConfig,TrigConfig,src_struct,path)

                fprintf('\n### Current camera sensor temperature : %.2f deg celcius\n', multi.Src.DeviceTemperature);

                imaqmex('feature', '-gigeDisablePacketResend', false);
                % Start Acquisition 
                multi.TiffWriter = Fast_BigTiff_Write(strcat([Fullpath,'.tiff']));
                multi.IsAcquiring = true;
                if strcmp(multi.Vid.Running,'off')
                    start(multi.Vid);
                end
                fprintf('\n### Acquire Mode Active!');
            end
        end
        
        function StopAcquiring(multi, DataFolder)
            fprintf('\n### Acquire Mode is being stopped...');
            multi.IsAcquiring = false;
            imaqmex('feature', '-gigeDisablePacketResend', true);
            multi.TiffWriter.close();
            fprintf('### Data log completed! Your files are saved in >> ..%s\n',DataFolder);
            if ~multi.IsMultiviewing
                stop(multi.Vid);
            end
        end
        
        function StartMultiviewing(multi,channels)
            % StartMultiviewing: Launches preview windows for each channel
            %   Creates the a figure for each channel, turns preview on (if
            %   off)
            multi.Imgs = gobjects(channels,1);
            multi.Figs = gobjects(channels,1);
            multi.Channels = channels;
            for i=1:channels
                [multi.Figs(i),multi.Imgs(i)] = makeFig(i,multi.Vid.VideoResolution,3);
            end
            multi.IsMultiviewing = true;
            if strcmp(multi.Vid.Running,'off')
                multi.Vid.FramesAcquiredFcnCount = channels;
                start(multi.Vid);
            else
                fprintf("\n### Currently can't start multiview while acquiring");
            end
        end
        
        function StopMultiviewing(multi)
            % StopMultiviewing: closes channel preview figues
            %   Closes figres, stops preview if not acquiring
            %   Also stops multiplexing if it is started
            if multi.IsMultiplexing
                multi.StopMultiplexing();
            end
            multi.IsMultiviewing = false;
            if ~multi.IsAcquiring
                try
                    stop(multi.Vid);
                catch
                end
            end
            for i=1:length(multi.Figs)
                try
                    close(multi.Figs(i));
                catch
                end
            end
            multi.Figs = gobjects(0,1);
            multi.Imgs = gobjects(0,1);
            multi.Channels = 3;
        end
        
        function StartMultiplexing(multi)
            % StartMultiplexing: Opens a multiplex figure (merging 3 channels as chosen)
            if ~multi.IsMultiviewing
                fprintf("\n### Can't start multiplexing without multiviewing\");
                return;
            end
            multi.Muximg = makeMux(multi.Vid.VideoResolution,multi.Imgs);
            multi.IsMultiplexing = true;
        end
        
        function StopMultiplexing(multi)
            multi.IsMultiplexing = false;
            try
                close(multi.Muximg.Parent.Parent);
            catch
            end
            multi.Muximg = gobjects;
        end
        
        function WriteToDisk(multi,eventData)
            for i=1:size(eventData,4)
                d = eventData(:,:,:,i);
                multi.TiffWriter.WriteIMG(d');
            end
        end
        
        function delete(multi)
            if multi.IsAcquiring
                multi.StopAcquiring();
            end
            if multi.IsMultiplexing
                multi.StopMultiplexing();
            end
            if multi.IsMultiviewing
                multi.StopMultiviewing();
            end            
        end
    end
end

