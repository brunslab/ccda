# Camera Control and Data Acquisition (CCDA) V3

### A Matlab application for imaging using Allied Vision GoldEye G-032 SWIR camera and triggering with an Arduino Nano board

#### Supported environment:

- Windows 10
- Matlab 2019a

#### Installation instructions

1. Download and install [Matlab 2019a](https://uk.mathworks.com/downloads/).
2. Include Image Acquisition Toolbox

Assuming you haven't added too many additional toolboxes, this shouldn't take more than an hour or two on a modern PC.

#### User instructions

**To download a release version** choose the appropriate tag and either clone and check it out, or simply download it.

**To start the application** run **CCDA_V300.mlapp**. You can use it to:

- preview or acquire images from the camera 
- split the preview into **n** "channels" , where frame **k** on channel **i** is frame **(k*n+i)**, as well as multiplex 2 or 3 of them into an RGB color image
- load an Arduino sketch and upload it to the Arduino board *(you still need to pick the appropriate COM port)*
- start the Arduino loop if using the setup function from the provided example in the Arduino folder
- *(in progress)* program the Arduino board using a GUI

**To report bugs** please open appropriately tagged issues in the [repository](https://gitlab.com/brunslab/ccda/-/boards) 

#### Contributor instructions

If you'd like to contribute please have a look at the open issues either in [the public repository](https://gitlab.com/brunslab/ccda/-/boards) or in our old [internal one](https://ascgitlab.helmholtz-muenchen.de/hpc-bruns/ccda). We're in the process of migrating to the public repository.
