function [status,message] = TriggerArduino(comport_name)
%TriggerArduino Sends a signal to arduino board through the serial port, triggering the start of its loop.
    
% Camera Control and Data Acquisition (CCDA) V3
% Copyright (C) 2020  BrunsLab HPC
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <https://www.gnu.org/licenses/>.
    try
        ard = serial(comport_name,'BaudRate',9600);
%         pause(5);
        fopen(ard);
        pause(2);
        fprintf(ard,'1');
%         pause(5);
        fclose(ard);
        status = 0;
        message = 'Trigger arduino board Successful!';
    catch E
        try
            fclose(ard);
        catch
        end
        status = 1;
        message = E.message;
    end
end

