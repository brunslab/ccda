// <-- These backslashes signify a comment - everything on the same line
// after them will be ignored by compiler and won't be put on the Arduino

/* Camera Control and Data Acquisition (CCDA) V3
Copyright (C) 2020  BrunsLab HPC

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>. */

// Pin Assignments for TC_SWIR Circuitry (Check the pin assignement excel sheet)
// These are the pin numbers assumed in our lab. If you're changing
// something in the setup you can lookup which pins you're using on the
// board itself - look for a label like D2 (channel 2) next to the pin
const int L_785 = 2; // Laser 785
const int L_892 = 3; // Laser 892
const int L_980 = 4; // Laser 980
const int L_1064 = 5; // Laser 1064

// Camera pin 11 is Line2 on the G032 camera, the opto-isolated input. It is 
// an order slower (signal reaches in 3.6us instead of .6us) but supposedly
// less noisy than Line1 (pin 12)
const int Cam = 11; // Camera


// This is the loop - it defines 
void loop() {
  // the fullwidth parameter is the width of each channel in milliseconds
  // you are not bound to use it, but if you're triggering on rising edge
  // (instead of exposing on high), it is the better option
  int fullwidth = 12;

  // You call lasercam(<laser>,<channel_width>,<laser_ontime>) to turn on
  // the laser and trigger the camera acquisition. This keeps the laser on for
  // <laser_ontime> milliseconds and then waits <channel_width>-<laser_ontime>
  // milliseconds before continuing. Call for each laser separately. You can
  // call each laser multiple times in the same loop. The loop will continue
  // executing until 
  lasercam(L_785,fullwidth,10);
  //lasercam(L_892,fullwidth,200);
  lasercam(L_980,fullwidth,10);
  //lasercam(L_1064,fullwidth,200);
}

// Lasercam function - only change if you eg want to use delayMicroseconds
void lasercam(int laser_pin,int fullwidth,int laseron)
{
  digitalWrite(laser_pin, HIGH);
  digitalWrite(Cam, HIGH);
  delay(laseron);
  digitalWrite(Cam, LOW);
  digitalWrite(laser_pin, LOW);
  delay(fullwidth-laseron);
}

// setup function - don't touch this unless you know what you're doing
void setup() {

  // initialize digital pins as output
  pinMode(L_785, OUTPUT);
  pinMode(L_892, OUTPUT);
  pinMode(L_980, OUTPUT);
  pinMode(L_1064, OUTPUT);
  pinMode(Cam, OUTPUT);

  // Hold all Pins to low at first

  digitalWrite(Cam, LOW);
  digitalWrite(L_785, LOW);
  digitalWrite(L_892, LOW);
  digitalWrite(L_980, LOW);
  digitalWrite(L_1064, LOW);

  // Comment or delete the rest of this function if not starting/triggering Arduino from PC
  bool triggered = false;
  Serial.begin(9600);
  //digitalWrite(L_980,HIGH);
  while (!triggered)
  {
    //if (Serial.available() > 0) //this is what should have worked but doesn't
    //{
      char c = Serial.read();
      if (c=='1')
      {        
        triggered=true;
      }
    //}
  }
}