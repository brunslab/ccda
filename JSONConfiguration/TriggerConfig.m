function TriggerConfig = TriggerConfig(ArduinoFile)
%TRIGGERCONFIG Get Trigger Controller config data
% 
% Camera Control and Data Acquisition (CCDA) V3
% Copyright (C) 2020  BrunsLab HPC
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
    % along with this program.  If not, see <https://www.gnu.org/licenses/>.
    TriggerConfig.TargetHardware.Name = 'Trigger Controller V2.5';
    TriggerConfig.TargetHardware.Version = '2.5';
    TriggerConfig.TargetHardware.Vendor = 'Bruns Laboratory';
    TriggerConfig.TargetHardware.Date = '26.06.2019';
    TriggerConfig.TargetHardware.MCU = 'atmega16 (Arduino Nano V3)';

    if length(ArduinoFile)>0 && isfile(ArduinoFile)
        TriggerConfig.ArduinoCode = fileread(ArduinoFile);
    end                     
end

