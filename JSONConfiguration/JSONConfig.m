function JSONConfig(GenConfig,TrigConfig,src_struct,path)
%JSONCONFIG JSON Configuration of CCDA App
% Get configuration data and save to json

% Camera Control and Data Acquisition (CCDA) V3
% Copyright (C) 2020  BrunsLab HPC
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU General Public License as published by
% the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU General Public License for more details.
%
% You should have received a copy of the GNU General Public License
% along with this program.  If not, see <https://www.gnu.org/licenses/>.
    
    CCDAconfig.General = GenConfig;
    CCDAconfig.Camera = src_struct;
    CCDAconfig.TriggerController = TrigConfig;

    try
        jsoncode = jsonencode(CCDAconfig); % convert to jsoncode
        json_fid = fopen(path, 'w'); % open a json file in write mode
        fwrite(json_fid, jsoncode, 'char'); % save in file
        fclose(json_fid); % close json file
    catch MExc
        disp('\n### Attention! CCDA configurations could not be stored.');
        disp(MExc.message);
        disp('\n### JSON write skipped. \n');
    end
            