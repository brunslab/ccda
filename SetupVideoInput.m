function [vid,src] = SetupVideoInput(ColorDepth)
%SETUPVIDEOINPUT Sets up videoinput and source objects for G-032 camera
%   Default settings of the G-032 camera are set here

    % Camera Control and Data Acquisition (CCDA) V3
    % Copyright (C) 2020  BrunsLab HPC
    %
    % This program is free software: you can redistribute it and/or modify
    % it under the terms of the GNU General Public License as published by
    % the Free Software Foundation, either version 3 of the License, or
    % (at your option) any later version.
    %
    % This program is distributed in the hope that it will be useful,
    % but WITHOUT ANY WARRANTY; without even the implied warranty of
    % MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    % GNU General Public License for more details.
    %
    % You should have received a copy of the GNU General Public License
    % along with this program.  If not, see <https://www.gnu.org/licenses/>.

    AnalogGainMode = 'Gain1';
%     ColorDepth = 'Mono14';
    CameraExposureTime = 5000;
    SensorTempSetPoint = -20;
    NUCmode = 'Off';%'TwoPoint'; % Non-uniformity correction
    FramesPerTrigger = Inf;  

    vid = videoinput('gige', 1, ColorDepth);
    src = getselectedsource(vid);

    imaqmex('feature', '-gigeDisablePacketResend', true);

    vid.FramesPerTrigger = FramesPerTrigger;
    vid.TriggerRepeat = Inf;
    triggerconfig(vid,'immediate','none', 'none');
    
    src.ExposureTime = CameraExposureTime; 
    src.SensorGain = AnalogGainMode;
    
    % Packet size should supposedly be a bit smaller than the jumbo
    % packet size your Gigabit adaptor allows, one of ours limits us to 7k
    % but it's usually 9k
    src.PacketSize = 7000;
    src.PacketDelay = CalculatePacketDelay(vid, src.AcquisitionFrameRate);
    src.SensorTemperatureSetpointValue = SensorTempSetPoint; % Set camera temperature set-point

    % Non-uniformity correction setting
    % NUCDatasetAuto=Continuous means camera selects correct NUCDataset at 
    % every frame (depends on temp, gain and exp time)
    src.NUCDatasetAuto = 'Continuous'; 
    src.NUCMode = NUCmode;
    % Defect pixel correction
    src.DPCMode = 'On';
    % Background correction mode
    src.BCMode = 'Off';

    src.TriggerMode = 'Off';
end

